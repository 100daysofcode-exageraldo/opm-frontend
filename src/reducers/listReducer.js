import { CONSTANTS } from '../actions';

let listID = 2;

const initialState = [
    {
        title: "Lista um",
        id: 0,
        cards: [
            {
                id: 0,
                text: "Card um da lista um"
            },
            {
                id: 1,
                text: "Card dois da lista um"
            }
        ]
    },
    {
        title: "Lista dois",
        id: 1,
        cards: [
            {
                id: 0,
                text: "Card um da lista dois"
            },
            {
                id: 1,
                text: "Card dois da lista dois"
            }
        ]
    }
];

const listReducer = (state=initialState, action) => {
    switch(action.type) {
        case CONSTANTS.ADD_LIST:
            const newList = {
                title: action.payload.title,
                card: [],
                id: listID
            };
            listID += 1;
            return [...state, newList];
        default:
            return state;
    }
};

export default listReducer;