import React from 'react';
import TaskCard from './TaskCard';
import AddActionButton from './AddActionButton';


const TaskList = ({ title, cards }) => (
    <div style={styles.container}>
        <h3>{title}</h3>
        { cards.map(card => (
            <TaskCard key={card.id} text={card.text} />
        ))}
        <AddActionButton />
    </div>
);

const styles = {
    container: {
        backgroundColor:"#ccc",
        borderRadius: 3,
        width: 300,
        padding: 8,
        height: '100%',
        marginRight: 8
    }
}

export default TaskList;