import React from 'react';
import Icon from '@material-ui/core/Icon';
import Card from '@material-ui/core/Card';
import Textarea from 'react-textarea-autosize';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { addList } from '../actions';


class AddActionButton extends React.Component {
    state = {
        formOpen: false
    };

    openForm = () => {
        this.setState({
            formOpen: true
        });
    }

    closeForm = () => {
        this.setState({
            formOpen: false
        });
    }

    handleAddList = () => {
        const { dispatch } = this.props;
        const { text } = this.state;

        if(text) {
            dispatch(addList(text));
        }
        
        return;
    }

    renderAddButton = () => {
        const { list } = this.props;
        
        const buttonText = list ? 'list' : 'card';
        const buttonOpacity = list ? 1 : 0.5;
        const buttonTextColor = list ? 'white' : 'inherit';
        const buttonTextBackground = list ? 'rgba(0,0,0,.15)' : 'inherit';

        return (
            <div
                onClick={this.openForm} 
                style={{
                    ...styles.openFormButtonGroup,
                    opacity: buttonOpacity,
                    color: buttonTextColor,
                    backgroundColor: buttonTextBackground
                }}
            >
                <Icon>add</Icon>
                <p>Add another {buttonText}</p>
            </div>
        )
    };

    handleInputChange = (e) => {
        this.setState({
            text: e.target.value
        });
    };

    renderForm = () => {
        const { list } = this.props;
        const placeholder = list
            ? 'Enter list title...'
            : 'Enter a cart info...';
        const buttonTitle = list
            ? 'Add List'
            : 'Add Card';
        return (
            <div>
                <Card
                    style={{
                        minHeight: 80,
                        minWidth: 272,
                        padding: '6px 8px 2px'
                    }}
                >
                    <Textarea
                        placeholder={placeholder}
                        onBlur={this.closeForm}
                        value={this.state.text}
                        onChange={this.handleInputChange}
                        autoFocus
                        style={{
                            resize: 'none',
                            width: '100%',
                            outline: 'none',
                            border: 'none'
                        }}
                    />
                </Card>
                <div style={styles.formButtonGroup}>
                    <Button
                        onMouseDown={this.handleAddList}
                        variant='contained'
                        style={{
                            color: 'white',
                            backgroundColor: '#5aac44'
                        }}
                    >
                        {buttonTitle}
                    </Button>
                    <Icon
                        style={{
                            marginLeft: 8,
                            cursor: 'pointer'
                        }}
                    >
                        close
                    </Icon>
                </div>
            </div>
        );
    };

    render() {
        return this.state.formOpen ? this.renderForm(): this.renderAddButton();
    };
}


const styles = {
    openFormButtonGroup: {
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        borderRadius: 3,
        height: 36,
        width: 272,
        paddingLeft: 10
    },
    formButtonGroup: {
        marginTop: 8,
        display: 'flex',
        alignItems: 'center'
    }
}

export default connect()(AddActionButton);