import React from 'react';
import TaskList from './TaskList';
import { connect } from 'react-redux';
import AddActionButton from './AddActionButton';


class App extends React.Component {

  render() {
    const { lists } = this.props;
    return(
      <div className="App">
        <h2>Fala Galera!</h2>
        <div style={styles.listContainer}>
          { lists.map(list => (
            <TaskList key={list.id} title={list.title} cards={list.cards}/>
          ))}
          <AddActionButton list />
        </div>
      </div>
    );
  }
}

const styles = {
  listContainer: {
    display: "flex",
    flexDirection: "row"
  }
}

const mapStateToProps = state => ({
  lists: state.lists
});

export default connect(mapStateToProps)(App);
